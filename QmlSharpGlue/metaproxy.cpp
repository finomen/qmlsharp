#include <private/qmetaobjectbuilder_p.h>
#include <QGuiApplication>
#include <QDebug>

#include "metaproxy.h"

#include<iostream>

MetaProxy::MetaProxy(QMetaObject * metaObject, int (*metacallCallback)(QMetaObject::Call, int, void**)) : mo(metaObject), metacallCallback(metacallCallback)
{
	moveToThread(QGuiApplication::instance()->thread());
}

const QMetaObject *MetaProxy::metaObject() const
{
    return mo;
}

void *MetaProxy::qt_metacast(const char * name)
{
    std::cout << "Metacast to " << name << std::endl;
    if (name == mo->className())
        return this;
    return nullptr; //TODO: implement
}

int MetaProxy::qt_metacall(QMetaObject::Call c, int id, void** argv)
{
    return metacallCallback(c, id, argv);
}
