#ifndef METAPROXY_H
#define METAPROXY_H

#include <functional>
#include <QString>
#include <QObject>
#include <QMetaObject>


class MetaProxy : public QObject
{
public:
    MetaProxy(QMetaObject * metaObject, int (*metacallCallback)(QMetaObject::Call, int, void**));
    virtual const QMetaObject *metaObject() const;
    virtual void *qt_metacast(const char *);
    virtual int qt_metacall(QMetaObject::Call, int, void**);
private:
    QMetaObject * mo;
    int (*metacallCallback)(QMetaObject::Call, int, void**);
};

#endif // METAPROXY_H
