#include <QtGlobal>
#include <QString>
#include "../exceptions.h"
#include <iostream>

extern "C"
{

Q_DECL_EXPORT QString * QString_New(const char * v)
{
	try {
    return new QString(QString::fromLocal8Bit(v));
	}
	catch (std::exception const & e) {
		SetException(e);
	}
	return nullptr;
}

Q_DECL_EXPORT void QString_Delete(QString * ptr)
{
	try {
    delete ptr;
	}
	catch (std::exception const & e) {
		SetException(e);
	}
}

Q_DECL_EXPORT void QString_Set(QString * str, const char * v)
{
	try {
		(*str) = QString::fromUtf8(v);
	}
	catch (std::exception const & e) {
		SetException(e);
	}
}

}
