#include <QtGlobal>
#include <private/qmetaobjectbuilder_p.h>
#include "../exceptions.h"

extern "C"
{

Q_DECL_EXPORT QMetaObjectBuilder * QMetaObjectBuilder_New()
{
	try {
    return new QMetaObjectBuilder();
	}
	catch (std::exception const & e) {
		SetException(e);
	}
	return nullptr;
}

Q_DECL_EXPORT void QMetaObjectBuilder_Delete(QMetaObjectBuilder * ptr)
{
	try {
    delete ptr;
	}
	catch (std::exception const & e) {
		SetException(e);
	}
}

Q_DECL_EXPORT void QMetaObjectBuilder_SetClassName(QMetaObjectBuilder * ptr, const char * className)
{
	try {
    ptr->setClassName(className);
	}
	catch (std::exception const & e) {
		SetException(e);
	}
}

Q_DECL_EXPORT void QMetaObjectBuilder_SetSuperClass(QMetaObjectBuilder * ptr, QMetaObject * super)
{
	try {
    ptr->setSuperClass(super);
	}
	catch (std::exception const & e) {
		SetException(e);
	}
}

Q_DECL_EXPORT QMetaPropertyBuilder * QMetaObjectBuilder_AddProperty(QMetaObjectBuilder * ptr, const char * name, const char * type)
{
	try {
    return new QMetaPropertyBuilder(ptr->addProperty(name, type));
	}
	catch (std::exception const & e) {
		SetException(e);
	}
	return nullptr;
}

Q_DECL_EXPORT QMetaMethodBuilder * QMetaObjectBuilder_AddSignal(QMetaObjectBuilder * ptr, const char * signature)
{
	try {
    return new QMetaMethodBuilder(ptr->addSignal(signature));
	}
	catch (std::exception const & e) {
		SetException(e);
	}
	return nullptr;
}

Q_DECL_EXPORT QMetaObject * QMetaObjectBuilder_ToMetaObject(QMetaObjectBuilder * ptr)
{
	try {
    return ptr->toMetaObject();
	}
	catch (std::exception const & e) {
		SetException(e);
	}
	return nullptr;
}

}
