#include <QtGlobal>
#include <private/qmetaobjectbuilder_p.h>
#include "../exceptions.h"

extern "C"
{



Q_DECL_EXPORT void QMetaPropertyBuilder_SetWriteable(QMetaPropertyBuilder * ptr, bool writeable)
{
	try {
    ptr->setWritable(writeable);
	}
	catch (std::exception const & e) {
		SetException(e);
	}
}

Q_DECL_EXPORT void QMetaPropertyBuilder_SetReadable(QMetaPropertyBuilder * ptr, bool readable)
{
	try {
    ptr->setReadable(readable);
	}
	catch (std::exception const & e) {
		SetException(e);
	}
}

Q_DECL_EXPORT void QMetaPropertyBuilder_SetNotifySignal(QMetaPropertyBuilder * ptr, QMetaMethodBuilder * sig)
{
	try {
    ptr->setNotifySignal(*sig);
	}
	catch (std::exception const & e) {
		SetException(e);
	}
}

Q_DECL_EXPORT void QMetaPropertyBuilder_Delete(QMetaPropertyBuilder * ptr)
{
	try {
    delete ptr;
	}
	catch (std::exception const & e) {
		SetException(e);
	}
}

}
