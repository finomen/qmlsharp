#include <QtGlobal>
#include <private/qmetaobjectbuilder_p.h>
#include "../exceptions.h"

extern "C"
{

Q_DECL_EXPORT void QMetaMethodBuilder_Delete(QMetaMethodBuilder * ptr)
{
	try {
		delete ptr;
	} 
	catch (std::exception const & e) {
		SetException(e);
	}
}

}
