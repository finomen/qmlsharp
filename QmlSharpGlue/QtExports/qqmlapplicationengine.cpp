#include <QtGlobal>
#include <QQmlApplicationEngine>
#include "../exceptions.h"
#include <iostream>
extern "C"
{

Q_DECL_EXPORT QQmlApplicationEngine * QQmlApplicationEngine_New()
{
	try {
    return new QQmlApplicationEngine();
	}
	catch (std::exception const & e) {
		SetException(e);
	}
	return nullptr;
}

Q_DECL_EXPORT void QQmlApplicationEngine_Delete(QQmlApplicationEngine * ptr)
{
	try {
    delete ptr;
	}
	catch (std::exception const & e) {
		SetException(e);
	}
}

Q_DECL_EXPORT void QQmlApplicationEngine_Load(QQmlApplicationEngine * ptr, const char * path)
{
    try {
		ptr->load(QUrl(path));
	}
	catch (std::exception const & e) {
		SetException(e);
	}
}

Q_DECL_EXPORT QQmlContext * QQmlApplicationEngine_RootContext(QQmlApplicationEngine * ptr)
{
	try {
    return ptr->rootContext();
	}
	catch (std::exception const & e) {
		SetException(e);
	}
	return nullptr;
}

}
