#include <QtGlobal>
#include <QTranslator>
#include "../exceptions.h"

extern "C"
{
	class QProxyTranslator : public QTranslator
	{
	public:
		QProxyTranslator(void (*callback)(QString *, const char *, const char *, const char *, int)) : callback(callback)
		{

		}

		virtual bool isEmpty() const override
		{
			return false;
		}

		virtual QString translate(const char * context, const char * sourceText, const char *disambiguation = Q_NULLPTR, int n = -1) const override
		{
			QString res;
			callback(&res, context, sourceText, disambiguation, n);
			return res;
		}
	private:
		void(*callback)(QString *, const char *, const char *, const char *, int);
	};

	Q_DECL_EXPORT QProxyTranslator * QTranslator_New(void(*callback)(QString *, const char *, const char *, const char *, int))
	{
		try {
			return new QProxyTranslator(callback);
		}
		catch (std::exception const & e) {
			SetException(e);
		}
		return nullptr;
	}

	Q_DECL_EXPORT void QTranslator_Delete(QTranslator * ptr)
	{
		try {
			delete ptr;
		}
		catch (std::exception const & e) {
			SetException(e);
		}
	}
}
