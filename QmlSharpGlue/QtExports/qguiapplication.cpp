#include <QtGlobal>
#include <QGuiApplication>
#include "../exceptions.h"
#include <iostream>
extern "C"
{

Q_DECL_EXPORT QGuiApplication * QGuiApplication_New(int argc, const char * argv[])
{
	auto nargv = new char *[argc];
    auto nargc = new int;
    *nargc = argc;
	for (auto i = 0; i < argc; ++i)
	{
		nargv[i] = new char[strlen(argv[i]) + 1];
		strcpy(nargv[i], argv[i]);
        std::cout << "ARG " << i << " = " << nargv[i] << std::endl;
	}

    QGuiApplication * res = nullptr;

	try {
		res = new QGuiApplication(*nargc, nargv);
	}
	catch (std::exception const & e) {
		SetException(e);
	}


	return res;
}

Q_DECL_EXPORT void QGuiApplication_Delete(QGuiApplication * ptr)
{
	try {
    delete ptr;
	}
	catch (std::exception const & e) {
		SetException(e);
	}
}

Q_DECL_EXPORT void QGuiApplication_Exec(QGuiApplication * app)
{
	try {
    app->exec();
	}
	catch (std::exception const & e) {
		SetException(e);
	}
}

Q_DECL_EXPORT void QGuiApplication_InstallTranslator(QGuiApplication * app, QTranslator * translator)
{
	try {
		app->installTranslator(translator);
	}
	catch (std::exception const & e) {
		SetException(e);
	}
}

}
