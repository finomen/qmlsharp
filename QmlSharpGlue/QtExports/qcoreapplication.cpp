#include <QtGlobal>
#include <QCoreApplication>


extern "C" {

Q_DECL_EXPORT void QCoreApplication_setAttribute(int attr)
{
    QCoreApplication::setAttribute((Qt::ApplicationAttribute)attr);
}

}
