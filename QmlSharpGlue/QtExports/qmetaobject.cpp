#include <QtGlobal>
#include <QObject>
#include <QMetaObject>
#include "../exceptions.h"

extern "C"
{

Q_DECL_EXPORT const QMetaObject * QMetaObject_QObjectStaticMetaObject()
{
    return &QObject::staticMetaObject;
}

Q_DECL_EXPORT void QMetaObject_Delete(QMetaObject * ptr)
{
	try {
    delete ptr;
	}
	catch (std::exception const & e) {
		SetException(e);
	}
}

Q_DECL_EXPORT void QMetaObject_Activate(QObject * ptr, int id, void ** arg)
{
	try {
    QMetaObject::activate(ptr, ptr->metaObject(), id, arg);
	}
	catch (std::exception const & e) {
		SetException(e);
	}
}

}
