#include <QtGlobal>
#include <QQmlContext>
#include "../exceptions.h"
extern "C" {

Q_DECL_EXPORT void QQmlContext_SetContextProperty(QQmlContext * ctx, const char * name, QObject * obj)
{
	try {
		ctx->setContextProperty(name, obj);
	}
	catch (std::exception const & e) {
		SetException(e);
	}
}

}
