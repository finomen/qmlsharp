#ifndef _H_EXCEPTIONS
#define _H_EXCEPTIONS

#include <exception>

void SetException(std::exception const & e);

#endif
