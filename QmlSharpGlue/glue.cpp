#include <QString>
#include "metaproxy.h"
#include "exceptions.h"
#include <stack>

namespace {
	thread_local std::stack<void(*)(const char *)> exceptionHandlers;
}

extern "C"
{
Q_DECL_EXPORT MetaProxy * MetaProxy_New(QMetaObject * metaObject, int (*metacallCallback)(QMetaObject::Call, int, void**))
{
	try {
		return new MetaProxy(metaObject, metacallCallback);
	}
	catch (std::exception const & e) {
		SetException(e);
	}

	return nullptr;
}

Q_DECL_EXPORT void MetaProxy_Delete(MetaProxy * proxy)
{
	try {

		delete proxy;
	}
	catch (std::exception const & e) {
		SetException(e);
	}
}

Q_DECL_EXPORT void SetExceptionCallback(void(*exceptionCallback)(const char *))
{
	exceptionHandlers.push(exceptionCallback);
}

Q_DECL_EXPORT void ResetExceptionCallback()
{
	exceptionHandlers.pop();
}

}

void SetException(std::exception const & e)
{
	if (!exceptionHandlers.empty())
		exceptionHandlers.top()(e.what());
}

