﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using QmlSharp.Qt;

namespace QmlSharp
{
    internal class MetaProxy
    {
        private class PropertyWrapper
        {
            internal PropertyWrapper(MetaProxy obj, PropertyInfo pi, int sigId)
            {
                this.obj = obj;
                this.pi = pi;
                this.sigId = sigId;
            }

            internal unsafe void Get(IntPtr argv)
            {
                var t = pi.PropertyType;
                var value = pi.GetValue(obj.obj);
                var p = argv.ToPointer();
                if (t == typeof(string))
                {
                    new QString(argv).Set(value as string);
                } else if (t.IsPrimitive)
                {
                    if (t == typeof(int))
                    {
                        *(int*) p = (int) value;
                    }
                    else if (t == typeof(double))
                    {
                        *(double*) p = (double) value;
                    }
                    else if (t == typeof(bool))
                    {
                        *(bool*)p = (bool)value;
                    }
                    else
                    {
                        throw new NotImplementedException();
                    }
                }
                else
                {
                    if (wrapper == null || !wrapper.Is(value))
                    {
                        wrapper = new MetaProxy(value, t);
                    }
                    *(IntPtr*) (argv.ToPointer()) = wrapper.Handle;
                }

            }

            private unsafe void ActivatePrimitive<T>(object value) 
            {
                IntPtr[] args = new IntPtr[2];
                args[0] = IntPtr.Zero;

                var reference = __makeref(value);
                args[1] = *(System.IntPtr*)(&reference);
                QMetaObject.Activate(obj, sigId, args);
            }

            internal unsafe void HandleChanged()
            {
                var t = pi.PropertyType;

                var value = pi.GetValue(obj.obj);

                if (t == typeof(string))
                {
                    IntPtr[] args = new IntPtr[2];
                    args[0] = IntPtr.Zero;
                    var s = new QString(value as string);
                    args[1] = s.Handle;
                    QMetaObject.Activate(obj, sigId, args);
                }
                else if (t.IsPrimitive)
                {
                    GetType()
                        .GetMethod("ActivatePrimitive", BindingFlags.NonPublic | BindingFlags.Instance)
                        .MakeGenericMethod(t)
                        .Invoke(this, new object[] {value});
                }
                else
                {
                    IntPtr[] args = new IntPtr[2];
                    args[0] = IntPtr.Zero;
                    if (wrapper == null || !wrapper.Is(value))
                    {
                        wrapper = new MetaProxy(value, t);
                    }
                    args[1] = wrapper.Handle;
                    QMetaObject.Activate(obj, sigId, args);
                }
            }


            internal PropertyInfo pi;
            internal MetaProxy obj;
            internal int sigId;
            internal MetaProxy wrapper;
        }

        [DllImport(Constants.NativeLibName, CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr MetaProxy_New(IntPtr metaObject, QMetaObject.MetacallCallback metacallCallback);

        [DllImport(Constants.NativeLibName, CallingConvention = CallingConvention.Cdecl)]
        private static extern void MetaProxy_Delete(IntPtr ptr);

        internal bool Is(object obj)
        {
            return obj == this.obj;
        }

        private readonly QMetaObject.MetacallCallback metacallDelegate;
        
        internal unsafe MetaProxy(Object obj, Type type = null)
        {
            if (type == null)
                type = obj.GetType();
            var mo_bldr = new QMetaObjectBuilder();
            var cls = TypeHelper.MakeName(type);
            mo_bldr.SetClassName(cls);
            mo_bldr.SetSuperClass(QMetaObject.QObject);


            if (obj is INotifyPropertyChanged)
            {
                (obj as INotifyPropertyChanged).PropertyChanged += OnPropertyChanged;
            }

            int id = 1;
            int sigId = 0;
            foreach (var p in type.GetProperties())
            {
                var t = TypeHelper.GetTypeName(p.PropertyType);
                var chSig = mo_bldr.AddSignal("void(" + t +")");
                
                var pbld = mo_bldr.AddProperty(p.Name, t);
                pbld.SetWriteable(p.CanWrite);
                pbld.SetReadable(p.CanRead);
                pbld.SetNotifySignal(chSig);

                propertyIds[p.Name] = id;
                properties[id++] = new PropertyWrapper(this, p, sigId);
                ++sigId;
            }


            this.metaObject = mo_bldr.ToMetaObject();
            metacallDelegate = QtMetacall;
            self = MetaProxy_New(metaObject.Handle, metacallDelegate);
            this.obj = obj;
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            var prop = propertyChangedEventArgs.PropertyName;
            if (propertyIds.ContainsKey(prop))
            {
                var id = propertyIds[prop];
                properties[id].HandleChanged();
            }
        }

        private unsafe int QtMetacall(QMetaObject.Call call, int id, IntPtr * argv)
        {
            switch (call)
            {
                    case QMetaObject.Call.ReadProperty:
                    if (properties.ContainsKey(id))
                    {
                        properties[id].Get(argv[0]);
                        return 0;  
                    }
                    break;
            }
            return -1;
        }

        internal unsafe MetaProxy(QMetaObject metaObject)
        {
            this.metaObject = metaObject;
            var ex = new ExceptionWrapper();
            self = MetaProxy_New(metaObject.Handle, QtMetacall);
            ex.Finish();
        }

        ~MetaProxy()
        {
            var ex = new ExceptionWrapper();
            MetaProxy_Delete(self);
            ex.Finish();
        }

        private Dictionary<string, int> propertyIds = new Dictionary<string, int>();
        private Dictionary<int, PropertyWrapper> properties = new Dictionary<int, PropertyWrapper>();

        internal IntPtr Handle => self;

        private QMetaObject metaObject;
        private IntPtr self;
        private object obj;
    }
}
