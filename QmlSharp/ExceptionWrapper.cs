﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace QmlSharp
{
    internal class ExceptionWrapper
    {
        private string message = null;

        private delegate void ExceptionCallback(string message);

        [DllImport(Constants.NativeLibName, CallingConvention = CallingConvention.Cdecl)]
        private static extern void SetExceptionCallback(ExceptionCallback callback);

        [DllImport(Constants.NativeLibName, CallingConvention = CallingConvention.Cdecl)]
        private static extern void ResetExceptionCallback();

        internal ExceptionWrapper()
        {
            SetExceptionCallback(SetMessage);
        }

        private void SetMessage(string message)
        {
            this.message = message;
        }

        internal void Finish()
        {
            ResetExceptionCallback();
            if (message != null)
                throw new NativeException(message);
        }
    }
}
