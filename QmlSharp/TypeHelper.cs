﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using QmlSharp.Qt;

namespace QmlSharp
{
    internal class TypeHelper
    {
        internal static string GetTypeName(Type t)
        {
            if (t == typeof(string))
                return "QString";
            else if (t == typeof(int))
                return "int";
            else if (t == typeof(bool))
                return "bool";
            else if (t == typeof(double))
                return "double";
            else if (t.IsPrimitive)
                throw new ArgumentException("Type " + t.Name + " not supported");
            else
                return "QObject *";//TODO: maybe MakeName(t) + "";
        }

        internal static string MakeName(Type t)
        {
            return t.FullName.Replace("+", "::").Replace(".", "::");
        }

        internal static unsafe void Assign(IntPtr native, object obj, Type t)
        {
            if (t == typeof(string))
            {
                (new QString(native)).Set(obj as string);
            }
            else if (t == typeof(int))
            {
                *(int*) native.ToPointer() = (int)obj;
            }
            else if (t == typeof(bool))
            {
                *(bool*)native.ToPointer() = (bool)obj;
            }
            else if (t == typeof(double))
            {
                *(double*)native.ToPointer() = (double)obj;
            }
            else if (t.IsPrimitive)
                throw new ArgumentException("Type " + t.Name + " not supported");
            else
                throw new ArgumentException("Type " + t.Name + " not supported"); //TODO: meta-proxy
        }

    }
}
