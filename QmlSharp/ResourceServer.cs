﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Routing;

namespace QmlSharp
{
    class ResourceServer : IDisposable
    {
        private readonly HttpListener listener;
        

        public ResourceServer(string url)
        {
            listener = new HttpListener();
            listener.Prefixes.Add(url);
            listener.Start();
            listener.BeginGetContext(new AsyncCallback(ListenerCallback), listener);
        }

        private void ListenerCallback(IAsyncResult result)
        {
            HttpListener listener = (HttpListener)result.AsyncState;
            try
            {
                HttpListenerContext context = listener.EndGetContext(result);

                listener.BeginGetContext(new AsyncCallback(ListenerCallback), listener);

                HttpListenerRequest request = context.Request;
                HttpListenerResponse response = context.Response;

                var parts = request.RawUrl.Split('/').Where(x => x.Length > 0);
                var assemblyName = parts.First();
                var resource = String.Join(".", parts);
                var asm = AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault(x => x.GetName().Name == assemblyName);

                byte[] buffer;


                if (asm != null)
                {
                    try
                    {
                        var qmlSuf = ".qmldir";
                        if (resource.EndsWith(qmlSuf))
                        {
                            var prefix = resource.Substring(0, resource.Length - qmlSuf.Length);
                            var responseString = String.Join("\n", asm.GetManifestResourceNames().Where(x => x.EndsWith(".qml") && x.StartsWith(prefix)).Select(x =>
                            {
                                var ps = x.Split('.');
                                var type = ps[ps.Length - 2];
                                return type + " 1.0 " + type + ".qml";
                            }));
                            buffer = Encoding.UTF8.GetBytes(responseString);
                            response.StatusCode = 200;
                        }
                        else
                        {
                            using (var stream = asm.GetManifestResourceStream(resource))
                            {
                                if (stream != null)
                                {
                                    response.ContentLength64 = stream.Length;
                                    buffer = new byte[response.ContentLength64];
                                    stream.Read(buffer, 0, buffer.Length);
                                    response.StatusCode = 200;
                                }
                                else
                                {
                                    string responseString = "<HTML><BODY>Resource " + resource + " not found in assembly " +
                                                            asm.FullName + "</BODY></HTML>";
                                    buffer = Encoding.UTF8.GetBytes(responseString);
                                    response.StatusCode = 404;
                                }
                            }
                        }
                    }
                    catch (FileNotFoundException e)
                    {
                        string responseString = "<HTML><BODY>Resource " + resource + " not found in assembly " + asm.FullName + "</BODY></HTML>";
                        buffer = Encoding.UTF8.GetBytes(responseString);
                        response.StatusCode = 404;
                    }

                }
                else
                {
                    string responseString = "<HTML><BODY>Assembly " + assemblyName + " not found </BODY></HTML>";
                    buffer = Encoding.UTF8.GetBytes(responseString);
                    response.StatusCode = 404;
                }

                response.ContentLength64 = buffer.Length;

                using (var output = response.OutputStream)
                {
                    output.Write(buffer, 0, buffer.Length);
                }
            }
            catch (HttpListenerException)
            {
                return;
            }
        }


        public void Dispose()
        {
            listener.Stop();
        }

        
    }
}
