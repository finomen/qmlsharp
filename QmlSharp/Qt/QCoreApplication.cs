﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace QmlSharp.Qt
{
    public class QCoreApplication
    {
        static QCoreApplication()
        {
            Constants.Load();
        }

        [DllImport(Constants.NativeLibName, CallingConvention = CallingConvention.Cdecl)]
        private static extern void QCoreApplication_setAttribute(ApplicationAttribute attr);

        public static void SetAttribute(ApplicationAttribute attr)
        {
            var ex = new ExceptionWrapper();
            QCoreApplication_setAttribute(attr);
            ex.Finish();
        }
    }
}
