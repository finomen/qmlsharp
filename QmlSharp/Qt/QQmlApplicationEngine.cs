﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace QmlSharp.Qt
{
    public class QQmlApplicationEngine
    {
        [DllImport(Constants.NativeLibName, CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr QQmlApplicationEngine_New();

        [DllImport(Constants.NativeLibName, CallingConvention = CallingConvention.Cdecl)]
        private static extern void QQmlApplicationEngine_Delete(IntPtr ptr);

        [DllImport(Constants.NativeLibName, CallingConvention = CallingConvention.Cdecl)]
        private static extern void QQmlApplicationEngine_Load(IntPtr ptr, [MarshalAs(UnmanagedType.LPStr)]string path);

        [DllImport(Constants.NativeLibName, CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr QQmlApplicationEngine_RootContext(IntPtr ptr);

        public QQmlApplicationEngine()
        {
            var ex = new ExceptionWrapper();
            self = QQmlApplicationEngine_New();
            ex.Finish();
        }

        ~QQmlApplicationEngine()
        {
            if (self != IntPtr.Zero)
            {
                var ex = new ExceptionWrapper();
                QQmlApplicationEngine_Delete(self);
                ex.Finish();
            }
        }

        public void Load(string path) //TODO: support asbsolute url
        {
            var ex = new ExceptionWrapper();
            var asm = Assembly.GetCallingAssembly().GetName().Name;
            QQmlApplicationEngine_Load(self, Constants.Url + "/" + asm + "/" + path);
            ex.Finish();
        }

        public void Dispose()
        {
            var ex = new ExceptionWrapper();
            QQmlApplicationEngine_Delete(self);
            ex.Finish();
            self = IntPtr.Zero;
        }

        public QQmlContext RootContext => rootContext ?? (rootContext = new QQmlContext(QQmlApplicationEngine_RootContext(self))); //TODO handle exception

        private QQmlContext rootContext;
        private IntPtr self;
    }
}
