﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace QmlSharp.Qt
{
    internal class QMetaObjectBuilder
    {
        [DllImport(Constants.NativeLibName, CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr QMetaObjectBuilder_New();

        [DllImport(Constants.NativeLibName, CallingConvention = CallingConvention.Cdecl)]
        private static extern void QMetaObjectBuilder_Delete(IntPtr ptr);

        [DllImport(Constants.NativeLibName, CallingConvention = CallingConvention.Cdecl)]
        private static extern void QMetaObjectBuilder_SetClassName(IntPtr self, string name);

        [DllImport(Constants.NativeLibName, CallingConvention = CallingConvention.Cdecl)]
        private static extern void QMetaObjectBuilder_SetSuperClass(IntPtr self, IntPtr super);

        [DllImport(Constants.NativeLibName, CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr QMetaObjectBuilder_AddProperty(IntPtr self, string name, string type);

        [DllImport(Constants.NativeLibName, CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr QMetaObjectBuilder_AddSignal(IntPtr self, string signature);

        [DllImport(Constants.NativeLibName, CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr QMetaObjectBuilder_ToMetaObject(IntPtr self);

        internal QMetaObjectBuilder()
        {
            var ex = new ExceptionWrapper();
            self = QMetaObjectBuilder_New();
            ex.Finish();
        }

        internal void SetClassName(string name)
        {
            var ex = new ExceptionWrapper();
            QMetaObjectBuilder_SetClassName(self, name);
            ex.Finish();
        }

        internal void SetSuperClass(QMetaObject super)
        {
            var ex = new ExceptionWrapper();
            QMetaObjectBuilder_SetSuperClass(self, super.Handle);
            ex.Finish();
        }

        internal QMetaPropertyBuilder AddProperty(string name, string type)
        {
            var ex = new ExceptionWrapper();
            var res = new QMetaPropertyBuilder(QMetaObjectBuilder_AddProperty(self, name, type));
            ex.Finish();
            return res;
        }

        internal QMetaMethodBuilder AddSignal(string signature)
        {
            var ex = new ExceptionWrapper();
            var res = new QMetaMethodBuilder(QMetaObjectBuilder_AddSignal(self, signature));
            ex.Finish();
            return res;
        }

        internal QMetaObject ToMetaObject()
        {
            var ex = new ExceptionWrapper();
            var res = new QMetaObject(QMetaObjectBuilder_ToMetaObject(self));
            ex.Finish();
            return res;
        }

        ~QMetaObjectBuilder()
        {
            var ex = new ExceptionWrapper();
            QMetaObjectBuilder_Delete(self);
            ex.Finish();
        }

        
        private IntPtr self;
    }
}
