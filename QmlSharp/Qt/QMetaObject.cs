﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace QmlSharp.Qt
{
    internal class QMetaObject
    {
        internal enum Call
        {
            InvokeMetaMethod = 0,
            ReadProperty,
            WriteProperty,
            ResetProperty,
            QueryPropertyDesignable,
            QueryPropertyScriptable,
            QueryPropertyStored,
            QueryPropertyEditable,
            QueryPropertyUser,
            CreateInstance,
            IndexOfMethod,
            RegisterPropertyMetaType,
            RegisterMethodArgumentMetaType
        }

        internal unsafe delegate int MetacallCallback(Call c, int id, IntPtr * argv);

        [DllImport(Constants.NativeLibName, CallingConvention = CallingConvention.Cdecl)]
        private static extern void QMetaObject_Delete(IntPtr ptr);

        [DllImport(Constants.NativeLibName, CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr QMetaObject_QObjectStaticMetaObject();

        [DllImport(Constants.NativeLibName, CallingConvention = CallingConvention.Cdecl)]
        private static extern void QMetaObject_Activate(IntPtr ptr, int id, IntPtr[] arg);

        internal QMetaObject(IntPtr ptr, bool own = true)
        {
            this.own = own;
            self = ptr;
        }

        ~QMetaObject()
        {
            if (own)
            {
                var ex = new ExceptionWrapper();
                QMetaObject_Delete(self);
                ex.Finish();
            }
        }

        internal static QMetaObject QObject => qobject ?? (qobject = new QMetaObject(QMetaObject_QObjectStaticMetaObject(), false));

        internal IntPtr Handle { get { return self;  } }

        internal static void Activate(MetaProxy obj, int id, IntPtr[] args)
        {
            var ex = new ExceptionWrapper();
            QMetaObject_Activate(obj.Handle, id, args);
            ex.Finish();
        }

        private bool own;
        private IntPtr self;
        private static QMetaObject qobject;
    }
}
