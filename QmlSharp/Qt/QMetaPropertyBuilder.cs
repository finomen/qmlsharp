﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace QmlSharp.Qt
{
    internal class QMetaPropertyBuilder
    {
        [DllImport(Constants.NativeLibName, CallingConvention = CallingConvention.Cdecl)]
        private static extern void QMetaPropertyBuilder_Delete(IntPtr ptr);

        [DllImport(Constants.NativeLibName, CallingConvention = CallingConvention.Cdecl)]
        private static extern void QMetaPropertyBuilder_SetWriteable(IntPtr ptr, bool writeable);

        [DllImport(Constants.NativeLibName, CallingConvention = CallingConvention.Cdecl)]
        private static extern void QMetaPropertyBuilder_SetNotifySignal(IntPtr ptr, IntPtr signal);


        [DllImport(Constants.NativeLibName, CallingConvention = CallingConvention.Cdecl)]
        private static extern void QMetaPropertyBuilder_SetReadable(IntPtr ptr, bool readable);

        internal QMetaPropertyBuilder(IntPtr ptr)
        {
            self = ptr;
        }

        internal void SetWriteable(bool writeable)
        {
            var ex = new ExceptionWrapper();
            QMetaPropertyBuilder_SetWriteable(self, writeable);
            ex.Finish();
        }

        internal void SetReadable(bool readable)
        {
            var ex = new ExceptionWrapper();
            QMetaPropertyBuilder_SetWriteable(self, readable);
            ex.Finish();
        }

        internal void SetNotifySignal(QMetaMethodBuilder signal)
        {
            var ex = new ExceptionWrapper();
            QMetaPropertyBuilder_SetNotifySignal(self, signal.Handle);
            ex.Finish();
        }

        ~QMetaPropertyBuilder()
        {
            var ex = new ExceptionWrapper();
            QMetaPropertyBuilder_Delete(self);
            ex.Finish();
        }

        private IntPtr self;
    }
}
