﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace QmlSharp.Qt
{
    public class QString
    {
        [DllImport(Constants.NativeLibName, CallingConvention = CallingConvention.Cdecl)]
        private static extern void QString_Set(IntPtr str, byte[] v);

        [DllImport(Constants.NativeLibName, CallingConvention = CallingConvention.Cdecl)]
        private static extern void QString_Delete(IntPtr str);

        [DllImport(Constants.NativeLibName, CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr QString_New(byte[] v);

        internal QString(string value)
        {
            var ex = new ExceptionWrapper();
            var data = Encoding.UTF8.GetBytes(value ?? "");
            self = QString_New(data);
            ex.Finish();
            own = true;
        }

        internal QString(IntPtr ptr)
        {
            self = ptr;
            own = false;
        }

        ~QString()
        {
            if (own)
            {
                var ex = new ExceptionWrapper();
                QString_Delete(self);
                ex.Finish();
            }
        }

        public void Set(string v)
        {
            var ex = new ExceptionWrapper();
            var data = Encoding.UTF8.GetBytes(v ?? "");
            QString_Set(self, data);
            ex.Finish();
        }

        internal IntPtr Handle { get { return self; } }

        private bool own;
        private IntPtr self;
    }
}
