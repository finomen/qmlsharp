﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace QmlSharp.Qt
{
    public class QQmlContext
    {
        [DllImport(Constants.NativeLibName, CallingConvention = CallingConvention.Cdecl)]
        private static extern void QQmlContext_SetContextProperty(IntPtr ctx, string name, IntPtr obj);

        internal QQmlContext(IntPtr ptr)
        {
            self = ptr;
        }   

        public void SetContextProperty(string name, Object property)
        {
            var proxy = new MetaProxy(property);
            proxies[property] = proxy;
            var ex = new ExceptionWrapper();
            QQmlContext_SetContextProperty(self, name, proxy.Handle);
            ex.Finish();
        }

        private Dictionary<Object, MetaProxy> proxies = new Dictionary<object, MetaProxy>();
        private IntPtr self;
    }
}
