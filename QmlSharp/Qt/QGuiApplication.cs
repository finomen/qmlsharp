﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace QmlSharp.Qt
{
    public class QGuiApplication
    {
        [DllImport(Constants.NativeLibName, CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr QGuiApplication_New(int argc, /*[MarshalAs(UnmanagedType.LPStr)]*/string[] argv);

        [DllImport(Constants.NativeLibName, CallingConvention = CallingConvention.Cdecl)]
        private static extern void QGuiApplication_Delete(IntPtr ptr);

        [DllImport(Constants.NativeLibName, CallingConvention = CallingConvention.Cdecl)]
        private static extern void QGuiApplication_Exec(IntPtr ptr);

        [DllImport(Constants.NativeLibName, CallingConvention = CallingConvention.Cdecl)]
        private static extern void QGuiApplication_InstallTranslator(IntPtr ptr, IntPtr translator);

        public QGuiApplication(string[] args)
        {
            var ex = new ExceptionWrapper();
            self = QGuiApplication_New(args.Length, args);
            ex.Finish();
        }

        ~QGuiApplication()
        {
            if (self != IntPtr.Zero)
            {
                var ex = new ExceptionWrapper();
                QGuiApplication_Delete(self);
                ex.Finish();
            }
        }

        public void Exec()
        {
            using (var srv = new ResourceServer(Constants.Url))
            {
                var ex = new ExceptionWrapper();
                QGuiApplication_Exec(self);
                ex.Finish();
            }
        }

        private IntPtr self;

        public void Dispose()
        {
            var ex = new ExceptionWrapper();
            QGuiApplication_Delete(self);
            ex.Finish();
            self = IntPtr.Zero;
        }

        public void InstallTranslator(QTranslator translator)
        {
            this.translator = translator;
            QGuiApplication_InstallTranslator(self, translator.Handle);
        }

        private QTranslator translator;
    }
}
