﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace QmlSharp.Qt
{
    public abstract class QTranslator
    {
        private delegate void TranslateCallback(IntPtr res, [MarshalAs(UnmanagedType.LPStr)]string context, [MarshalAs(UnmanagedType.LPStr)]string sourceText, [MarshalAs(UnmanagedType.LPStr)]string disambiguation, int n);

        //void (*callback)(QString *, const char *, const char *, const char *, int)
        [DllImport(Constants.NativeLibName, CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr QTranslator_New(TranslateCallback callback);

        [DllImport(Constants.NativeLibName, CallingConvention = CallingConvention.Cdecl)]
        private static extern void QTranslator_Delete(IntPtr ptr);

        private readonly TranslateCallback callback;

        public QTranslator()
        {
            callback = Translate;
            self = QTranslator_New(callback);
        }

        private void Translate(IntPtr res, [MarshalAs(UnmanagedType.LPStr)]string context, [MarshalAs(UnmanagedType.LPStr)]string sourceText, [MarshalAs(UnmanagedType.LPStr)]string disambiguation, int i)
        {
            new QString(res).Set(Translate(context, sourceText, disambiguation, i));
        }

        ~QTranslator()
        {
            QTranslator_Delete(self);
        }

        public abstract string Translate(string context, string sourceText, string disambiguation, int i);

        internal IntPtr Handle { get { return self; }  }

        private IntPtr self;

    }
}
