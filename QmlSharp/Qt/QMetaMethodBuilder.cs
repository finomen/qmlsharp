﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace QmlSharp.Qt
{
    internal class QMetaMethodBuilder
    {
        [DllImport(Constants.NativeLibName, CallingConvention = CallingConvention.Cdecl)]
        private static extern void QMetaMethodBuilder_Delete(IntPtr ptr);

        internal QMetaMethodBuilder(IntPtr ptr)
        {
            self = ptr;
        }

        ~QMetaMethodBuilder()
        {
            var ex = new ExceptionWrapper();
            QMetaMethodBuilder_Delete(self);
            ex.Finish();
        }

        internal IntPtr Handle
        {
            get { return self; }
        }

        private IntPtr self;
    }
}
