﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace QmlSharp
{
    internal class Constants
    {
        internal const string WinResName = "QmlSharp.Natives.QmlSharpGlue.dll";

        internal const string WinLibName = "QmlSharpGlue.dll";

        internal static string TempLibName = null;

        internal const string NativeLibName = "QmlSharpGlue";

        static Constants()
        {
            Load();
        }

        internal static void Load()
        {
            if (TempLibName != null)
                return;
            string tempDirectory = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
            Directory.CreateDirectory(tempDirectory);

            var path = Path.Combine(tempDirectory, WinLibName); //TODO: linux!
            using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(WinResName))
            {
                using (var fStream = File.Create(path))
                {
                    stream.CopyTo(fStream);
                }
            }

            var oldPath = Environment.GetEnvironmentVariable("PATH");
            Environment.SetEnvironmentVariable("PATH", tempDirectory + ";" + oldPath);
            TempLibName = path;
        }
        
        internal const string Url = "http://127.0.0.1:9090/";
    }
}
