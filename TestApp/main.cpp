#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QDebug>
#include <QQmlContext>

#include "../QmlSharp/metaproxy.h"

extern "C"
{
QQmlApplicationEngine * QQmlApplicationEngine_New();
void QQmlApplicationEngine_Delete(QQmlApplicationEngine * ptr);
void QQmlApplicationEngine_Load(QQmlApplicationEngine * ptr, const char * path);

}

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine * engine = QQmlApplicationEngine_New();
    //engine->rootContext()->setContextProperty("name", null);
    QQmlApplicationEngine_Load(engine, "C:/Users/finom/QmlSharp/TestApp/main.qml");

    return app.exec();
}
