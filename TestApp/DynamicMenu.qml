﻿import QtQuick.Controls 1.4
import QtQml 2.2

Menu {
	id: dynamicMenu
	property var itemsModel : null


	Instantiator { 
		onObjectAdded: dynamicMenu.insertItem( index, object )  
		onObjectRemoved: dynamicMenu.removeItem( object )  
		model: dynamicMenu.itemsModel

		delegate: MenuItem {
			action: modelData
		}
	}
}