﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using QmlSharp.Qt;
using TestApp.Annotations;

namespace TestApp
{
    class Program
    {
        class Test2 : INotifyPropertyChanged
        {
            public event PropertyChangedEventHandler PropertyChanged;

            protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

            }

            public String StringProperty2
            {
                get { return label; }
                set
                {
                    if (label == value)
                        return;
                    label = value;
                    OnPropertyChanged();
                }
            }

            private String label;
        }
        
        class Test : INotifyPropertyChanged
        {
            private String label = "Hello 2";
            private String page = "page1.qml";
            private int intv = 2;
            private double doublev = 3.5;

            private Test2 obj = new Test2();
            private Test2 obj2 = new Test2();

            public Test2 ObjectProperty
            {
                get { return obj; }
            }

            public Test2 ObjectProperty2
            {
                get { return obj2; }
                set
                {
                    if (obj2 == value)
                        return;
                    obj2 = value;
                    OnPropertyChanged();
                }
            }

            public String StringProperty
            {
                get { return label; }
                set
                {
                    if (label == value)
                        return;
                    label = value;
                    OnPropertyChanged();
                }
            }

            public String Page
            {
                get { return page; }
                set
                {
                    if (page == value)
                        return;
                    page = value;
                    OnPropertyChanged();
                }
            }

            public int IntProperty
            {
                get { return intv; }
                set
                {
                    if (intv == value)
                        return;
                    intv = value;
                    OnPropertyChanged();
                }
            }

            public double DoubleProperty
            {
                get { return doublev; }
                set
                {
                    if (doublev == value)
                        return;
                    doublev = value;
                    OnPropertyChanged();
                }
            }



            public event PropertyChangedEventHandler PropertyChanged;

            [NotifyPropertyChangedInvocator]
            protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public class MyTranslator : QTranslator
        {
            public override string Translate(string context, string sourceText, string disambiguation, int i)
            {
                if (sourceText == "Page1")
                    return "Страница 1";
                return sourceText;
            }
        }

        [STAThread]
        static void Main(string[] args)
        {
            QCoreApplication.SetAttribute(ApplicationAttribute.AA_EnableHighDpiScaling);

			var l = new List<string> ();
	        l.Add("mono TestApp.exe");

			var app = new QGuiApplication(l.Union(args).ToArray());

            app.InstallTranslator(new MyTranslator());

            var engine = new QQmlApplicationEngine();

            var tst = new Test();
            tst.StringProperty = "Hello from C#";
            engine.RootContext.SetContextProperty("model", tst);
            engine.Load("main.qml");

            var t = new Thread(() =>
            {
                int v = 0;
                
                while(true)
                {
                    if (++v % 8 == 0)
                    {
                        tst.Page = "page" + ((v / 8) % 2 + 1) + ".qml";
                    }
                    Thread.Sleep(500);
                    tst.StringProperty = DateTime.Now.ToString();
                    ++tst.IntProperty;
                    tst.DoubleProperty += 0.1;
                    tst.ObjectProperty.StringProperty2 = "Test2: " + DateTime.Now.ToString();
                    var o = new Test2();
                    o.StringProperty2 = "Test2 new: " + DateTime.Now.ToString();
                    tst.ObjectProperty2 = o;
                }

            });
            t.Start();

            app.Exec();
            t.Abort();
            
            engine.Dispose();
            app.Dispose();
        }
    }
}
