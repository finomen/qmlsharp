#ifndef TESTSIGNAL_H
#define TESTSIGNAL_H

#include <QObject>

class TestSignal : public QObject
{
    Q_OBJECT
public:
    TestSignal();

signals:
    void signal1(int);
};

#endif // TESTSIGNAL_H
