import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")
	
	property list<Action> modes: [
		Action {
			//id: copyAction
			text: "Mode1"
			//shortcut: StandardKey.Copy
			iconName: "edit-copy"
			//enabled: (!!activeFocusItem && !!activeFocusItem["copy"])
			onTriggered: modeMenu.setMenu1()
		},
		Action {
			//id: copyAction
			text: "Mode2"
			//shortcut: StandardKey.Copy
			iconName: "edit-copy"
			//enabled: (!!activeFocusItem && !!activeFocusItem["copy"])
			onTriggered: modeMenu.setMenu2()
		}
	]

	property list<Action> m1: [
		Action {
			text: "M1A1"
			iconName: "edit-copy"
			onTriggered: modeMenu.setMenu1()
		},
		Action {
			text: "M1A2"
			iconName: "edit-copy"
			onTriggered: modeMenu.setMenu2()
		}
	]

	property list<Action> m2: [
		Action {
			text: "M2A1"
			iconName: "edit-copy"
			onTriggered: modeMenu.setMenu1()
		},
		Action {
			text: "M2A2"
			iconName: "edit-copy"
			onTriggered: modeMenu.setMenu2()
		}
	]

	menuBar: MenuBar  {
		DynamicMenu {
			title: "Menu 1"
			itemsModel: modes
		}

		DynamicMenu {
			title: "Menu 2"
			id: modeMenu

			function setMenu1()
			{
				modeMenu.itemsModel = m1;
			}

			function setMenu2()
			{
				modeMenu.itemsModel = m2;
			}
		}
		
	} 


    GridLayout {
    columns: 2

		Text { text: "String:" }
		Text { text: model.StringProperty }
		Text { text: "int:"}
		Text { text: model.IntProperty }
		Text { text: "double:"}
		Text { text: model.DoubleProperty }
		Text { text: "int+double:"}
        Text { text: model.IntProperty + model.DoubleProperty }
		Text { text: "String from child:" }
		Text { text: model.ObjectProperty.StringProperty2 }
		Text { text: "String from recreating child:" }
		Text { text: model.ObjectProperty2.StringProperty2 }

		Loader {
			Layout.columnSpan: 2
			id: pageLoader 
			source: model.Page
		}
	}
}
